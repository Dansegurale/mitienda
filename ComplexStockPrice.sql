SELECT TOP (100) PERCENT 
    dbo.ComplexStocks.Id AS ComplexStockId, 
    dbo.ComplexStocks.Units, 
    dbo.Products.Name AS Product,
    STRING_AGG(dbo.ProductOptions.Name,'-') WITHIN GROUP (ORDER BY dbo.ProductOptions.Name ASC) AS [Option], 
    STRING_AGG(dbo.ProductOptionValues.Value, '-') AS Value, 
    SUM(dbo.ProductOptionValues.AdditionalPrice) AS AddPrice
FROM dbo.ComplexStocks 
    INNER JOIN dbo.ProductOptionValueComplexStocks ON dbo.ComplexStocks.Id = dbo.ProductOptionValueComplexStocks.ComplexStock_Id 
    INNER JOIN dbo.ProductOptionValues ON dbo.ProductOptionValueComplexStocks.ProductOptionValue_Id = dbo.ProductOptionValues.Id 
    INNER JOIN dbo.ProductOptions ON dbo.ProductOptionValues.ProductOption_Id = dbo.ProductOptions.Id 
    INNER JOIN dbo.Products ON dbo.ProductOptions.Product_Id = dbo.Products.Id
GROUP BY dbo.ComplexStocks.Id, dbo.ComplexStocks.Units, dbo.Products.Name
ORDER BY ComplexStockId;



SELECT        dbo.Orders.Id AS OrderId, dbo.OrderLines.Id AS LineId, dbo.Orders.UserId, dbo.Orders.Date, dbo.Products.Name, STRING_AGG(dbo.ProductOptionValues.Value, '-') AS Value, SUM(dbo.ProductOptionValues.AdditionalPrice) 
                         + dbo.Products.Price AS 'Precio Unidad', dbo.OrderLines.Units
FROM            dbo.ProductOptionValues INNER JOIN
                         dbo.OrderLineProductOptionValues ON dbo.ProductOptionValues.Id = dbo.OrderLineProductOptionValues.POVId RIGHT OUTER JOIN
                         dbo.Orders INNER JOIN
                         dbo.OrderLines ON dbo.Orders.Id = dbo.OrderLines.Order_Id LEFT OUTER JOIN
                         dbo.Products ON dbo.Products.Id = dbo.OrderLines.Product_Id ON dbo.OrderLineProductOptionValues.OrderLineId = dbo.OrderLines.Id
GROUP BY dbo.Orders.Id, dbo.OrderLines.Id, dbo.Orders.UserId, dbo.Orders.Date, dbo.OrderLines.Units, dbo.Products.Price, dbo.Products.Name