﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiTienda;
using MiTienda.Controllers;
using MiTienda.Models;

namespace MiTienda.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            Category alimentos = new Category(1, "Alimentos");
            Category comida = new Category(2, "Comida", alimentos);
            Category bebida = new Category(3, "Bebida", alimentos);
            

            Product macarrones = new Product(1, "Macarrones", comida, "500g de macarrones elaborados segun la receta tradicional", 0.90f);
            Product macarrones2 = new Product(2, "Macarrones Tricolori", comida, "500g de macarrones de 3 colores", 1.20f);
            Product cola = new Product(3, "Cola", bebida, "Bebida refrescante a base de cola", 1.0f);
            Product fantaLimon = new Product(4, "Refresco de Limon", bebida, "Refrescante bebida de Limon de Murcia", 0.75f);
            Product fantaNaranja = new Product(5, "Refresco de Naranja", bebida, "Refrescante bebida de Naranja de Valencia", 0.75f);



            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
