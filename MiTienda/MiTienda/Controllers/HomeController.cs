﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MiTienda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiTienda.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            if (User.Identity.IsAuthenticated)
            {
                string id = User.Identity.GetUserId(); //using Microsoft.AspNet.Identity;
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var userManager = new UserManager<ApplicationUser>(
                        new UserStore<ApplicationUser>(db));

                    ApplicationUser usuario = userManager.FindById(id); //db.Users.Where(x => x.Id == id).FirstOrDefault();

                }

            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}