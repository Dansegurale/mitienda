namespace MiTienda.Migrations
{
    using MiTienda.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MiTienda.Models.ApplicationDbContext context)
        {

            //if (System.Diagnostics.Debugger.IsAttached == false) System.Diagnostics.Debugger.Launch();

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.


            //Categorias
            Category alimentos = new Category(1, "Alimentos");
            Category comida = new Category(2, "Comida", alimentos);
            Category bebida = new Category(3, "Bebida", alimentos);
            Category ropa = new Category(4, "Ropa");
            Category pSuperiores = new Category(5, "Prendas Superiores",ropa);
            Category pInferiores = new Category(6, "Prendas Inferiores",ropa);
            

            //Productos sin opciones
            Product macarrones = new Product("Macarrones", comida, "500g de macarrones elaborados segun la receta tradicional", 0.90f);
            Product macarrones2 = new Product("Macarrones Tricolori", comida, "500g de macarrones de 3 colores", 1.20f);
            Product cola = new Product("Cola", bebida, "Bebida refrescante a base de cola", 1.0f);
            Product fantaLimon = new Product("Refresco de Limon", bebida, "Refrescante bebida de Limon de Murcia", 0.75f);
            Product fantaNaranja = new Product("Refresco de Naranja", bebida, "Refrescante bebida de Naranja de Valencia", 0.75f);
            macarrones.Stock = new Stock(1, 50);
            macarrones2.Stock = new Stock(2, 50);
            cola.Stock = new Stock(3, 200);
            fantaLimon.Stock = new Stock(4, 100);
            fantaNaranja.Stock = new Stock(5, 100);


            //Productos con Opciones
            Product sudadera = new Product("Sudadera", pSuperiores, "Sudadera de algodon", 12.0f);

            ProductOption po1 = new ProductOption(sudadera,"Color");
            po1.Values.Add(new ProductOptionValue(po1, "Rojo", 0.0f));
            po1.Values.Add(new ProductOptionValue(po1, "Dorado", 0.50f));

            ProductOption po2 = new ProductOption(sudadera, "Talla");
            po2.Values.Add(new ProductOptionValue(po2, "S", 0.0f));
            po2.Values.Add(new ProductOptionValue(po2, "L", 0.50f));
            po2.Values.Add(new ProductOptionValue(po2, "XL", 1.0f));
                       
            List<ComplexStock> complexStocks = new List<ComplexStock>();
            int i = 0;
            foreach (ProductOptionValue pov1 in po1.Values)
            {
                foreach (ProductOptionValue pov2 in po2.Values)
                {
                    ComplexStock aux = new ComplexStock(++i,10);
                    aux.ProductOptionValues.Add(pov1);
                    aux.ProductOptionValues.Add(pov2);
                    complexStocks.Add(aux);
                }
            }
            ComplexStock[] cs = complexStocks.ToArray();

            Product tejanos = new Product("Tejanos", pInferiores, "Pantalones tejanos unisex", 24.50f);
            ProductOption po3 = new ProductOption(tejanos, "Talla");
            po3.Values.Add(new ProductOptionValue(po3, "S", 0.0f));
            po3.Values.Add(new ProductOptionValue(po3, "L", 0.50f));
            po3.Values.Add(new ProductOptionValue(po3, "XL", 1.0f));
            po3.Values.Add(new ProductOptionValue(po3, "XXL", 1.50f));

            Order o = new Order("0dad400d-c84d-489e-b9fa-543b8ea083e1");
            o.OrderLines.Add(new OrderLine(o,macarrones.Stock,2));
            o.OrderLines.Add(new OrderLine(o,cs[5],2));

            int i2 = o.OrderLines.Last().OptionValues.Count;

            //AddOrUpdate
            context.Category.AddOrUpdate(
                alimentos, comida, bebida,ropa, pSuperiores, pInferiores
            );
            context.Product.AddOrUpdate(
                macarrones, macarrones2, cola, fantaLimon, fantaNaranja, sudadera, tejanos
            );
            context.ProductOption.AddOrUpdate(
                po1,po2,po3
            );

            context.ComplexStock.AddOrUpdate(cs);
            context.Order.AddOrUpdate(o);


        }
    }
}
