﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        public String Name { get; set; }
        public Category ParentCategory { get; set; }
        public bool Visibility { get; set; } = true;
        public List<Product> Products { get; set; }


        public Category() { Products = new List<Product>(); }
        public Category(int id, string name):this()
        {
            Id = id;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
        public Category(int id, string name, Category parentCategory):this(id,name)
        {
            ParentCategory = parentCategory ?? throw new ArgumentNullException(nameof(parentCategory));
        }
    }
}