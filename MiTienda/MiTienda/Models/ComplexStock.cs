﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class ComplexStock
    {
        public int Id { get; set; }
        [Min(0)]
        public int Units { get; set; }
        public List<ProductOptionValue> ProductOptionValues { get; set; }

        public ComplexStock()
        {
            ProductOptionValues = new List<ProductOptionValue>();
        }
        public ComplexStock(int units) : this()
        {
            Units = units;
        }
        public ComplexStock(int id, int units) : this(units)
        {
            Id = id;
        }
    }
}