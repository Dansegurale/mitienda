﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MiTienda.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public List<Order> Orders { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("MiTiendaConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductOption> ProductOption { get; set; }
        public DbSet<ProductOptionValue> ProductOptionValue { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<ComplexStock> ComplexStock { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderLine> OrderLine { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Relacion Category 1-M Products
            modelBuilder.Entity<Category>()
                .HasMany<Product>(c => c.Products)
                .WithRequired(p => p.Category)
                .WillCascadeOnDelete();

            //Relacion Product-ProductOption
            modelBuilder.Entity<Product>()
                .HasMany<ProductOption>(p => p.Options)
                .WithRequired(po => po.Product)
                .WillCascadeOnDelete();

            //Relacion ProductOption-ProductOptionValue
            modelBuilder.Entity<ProductOption>()
                .HasMany<ProductOptionValue>(po => po.Values)
                .WithRequired(pov => pov.ProductOption)
                .WillCascadeOnDelete();

            //Relacion Product-Stock
            modelBuilder.Entity<Product>()
                .HasOptional<Stock>(p => p.Stock)
                .WithOptionalDependent(s=>s.Product)
                .WillCascadeOnDelete();

            //Relacion ProductOptionValue-ComplexStock
            modelBuilder.Entity<ProductOptionValue>()
                .HasMany<ComplexStock>(pov => pov.Stocks)
                .WithMany(cs => cs.ProductOptionValues);

            //Relacion Order-OrderLine
            modelBuilder.Entity<Order>()
                .HasMany<OrderLine>(o => o.OrderLines)
                .WithRequired(ol => ol.Order)
                .WillCascadeOnDelete();

            //Relacion OrderLine-Product
            modelBuilder.Entity<OrderLine>()
                .HasOptional<Product>(ol => ol.Product);

            //Relacion OrderLine-ProductOptionValue
            modelBuilder.Entity<OrderLine>()
                .HasMany<ProductOptionValue>(ol => ol.OptionValues)
                .WithMany()
                .Map(m=> {
                    m.MapLeftKey("OrderLineId");
                    m.MapRightKey("POVId");
                    m.ToTable("OrderLineProductOptionValues");
                });

        }
    }
}