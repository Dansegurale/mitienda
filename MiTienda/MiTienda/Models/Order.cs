﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class Order
    {
        public int Id { get; set; }

        [NotMapped]
        private string _userId;
        [Required]
        public string UserId {
            get { return this._userId; }
            set {
                if (value == null || value == "") throw new ArgumentNullException(nameof(value));
                this._userId = value;
            }
        }
        
        public DateTime Date { get; set; }
        public float Total { get; set; }
        public List<OrderLine> OrderLines { get; set; }

        public Order()
        {
            Date = DateTime.Now;
            OrderLines = new List<OrderLine>();
        }
        public Order(string userId):this()
        {
            UserId = userId;
        }
    }
}