﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class OrderLine
    {

        public int Id { get; set; }
        public Order Order { get; set; }
        [NotMapped]
        private Product _product;
        public Product Product { get=>_product;
            set {
                _product = value;
                _price = (value==null)?0f:value.Price;
            }
        }
        
        public BindingList<ProductOptionValue> OptionValues { get; set; }
        public int Units { get; set; }

        private float _price;
        public float Price { get => _price; set { _price = value; } }
        private float _total;
        public float Total { get => _total; set { _total = value; } }

        public OrderLine()
        {
            OptionValues = new BindingList<ProductOptionValue>();
            OptionValues.ListChanged += new ListChangedEventHandler(CalcularPrecioHandler);
        }

        private void CalcularPrecioHandler(object sender, ListChangedEventArgs e)
        {
            CalcularPrecioLinea();
        }

        public OrderLine(Order order, Stock stock, int units) : this()
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));
            if(stock==null) throw new ArgumentNullException(nameof(stock));
            Units = units;

            Product = (Product)stock.Product.Clone();
            
            CalcularPrecioLinea();
        }
        public OrderLine(Order order, ComplexStock stock, int units) : this()
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));
            if (stock == null) throw new ArgumentNullException(nameof(stock));
            Units = units;
            
            ComplexStock cs = (ComplexStock)stock;
            foreach (ProductOptionValue pov in cs.ProductOptionValues)
            {
                ProductOptionValue p = (ProductOptionValue)pov.Clone();
                OptionValues.Add(p);
            }
            ProductOptionValue aux = (ProductOptionValue)OptionValues.First<ProductOptionValue>().Clone();
            Product = (Product)aux.ProductOption.Product.Clone();

            CalcularPrecioLinea();
        }


        
        public void CalcularPrecioLinea()
        {            
            if (Product != null)
            {
                _price = Product.Price;
                if (OptionValues.Count != 0)
                {
                    foreach (ProductOptionValue pov in OptionValues)
                    {
                        _price += pov.AdditionalPrice;
                    }
                }
                _total = Price * Units;
            }
            
        }

    }
}