﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class Product:ICloneable
    {
        

        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        public String Name { get; set; }
        [NotMapped]
        private Category _category;
        public Category Category { get=>_category;
            set {
                if (_category != null) { _category.Products.Remove(this); }
                _category = value;
                if (value != null) { _category.Products.Add(this); }
            }
        }
        [StringLength(200)]
        public String Description { get; set; }
        public String Img { get; set; }
        public float Price { get; set; }
        [NotMapped]
        private Stock _stock;
        public Stock Stock {
            get => _stock; 
            set {
                _stock = value ?? throw new ArgumentNullException(nameof(_stock));
                _stock.Product = this;
            }
        }
        public List<ProductOption> Options{ get; set; }

        public Product(){
            Options = new List<ProductOption>();
        }
        public Product(string name, Category category, string description, float price):this()
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Category = category;
            Description = description;
            Price = price;
        }
        public Product(string name, Category category, string description, float price, Stock stock) : this(name, category, description, price)
        {
            Stock = stock ?? throw new ArgumentNullException(nameof(stock));
        }
        public Product(int id, string name, Category category, string description, float price) : this(name,category,description,price)
        {
            Id = id;
        }
        public Product(int id, string name, Category category, string description, float price, Stock stock) : this(id,name, category, description, price)
        {
            Stock = stock ?? throw new ArgumentNullException(nameof(stock));
        }

        public object Clone()
        {
            return new Product(this.Id, this.Name, this.Category, this.Description, this.Price);
        }
    }

}