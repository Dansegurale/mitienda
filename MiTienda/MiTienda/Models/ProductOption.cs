﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class ProductOption
    {
        public int Id { get; set; }
        [NotMapped]
        private Product _product;
        public Product Product { get => _product;
            set
            {
                if (_product != null) { _product.Options.Remove(this); }
                _product = value;
                if (value != null) { _product.Options.Add(this); }
            }
        }
        [Required]
        [StringLength(30)]
        public String Name { get; set; }
        public List<ProductOptionValue> Values { get; set; }

        public ProductOption(){
            Values = new List<ProductOptionValue>();
        }
        public ProductOption(Product product, string name):this()
        {
            Product = product ?? throw new ArgumentNullException(nameof(product));
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
        public ProductOption(int id, Product product, string name):this(product, name)
        {
            Id = id;
        }
    }
}