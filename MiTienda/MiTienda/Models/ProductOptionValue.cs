﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class ProductOptionValue:ICloneable
    {
        public int Id { get; set; }
        [NotMapped]
        private ProductOption _productOption;
        public ProductOption ProductOption
        {
            get => _productOption;
            set
            {
                if (_productOption != null) { _productOption.Values.Remove(this); }
                _productOption = value;
                if (value != null) { _productOption.Values.Add(this); }
            }
        }
        [Required]
        [StringLength(30)]
        public String Value { get; set; }
        public float AdditionalPrice { get; set; }
        public List<ComplexStock> Stocks { get; set; }

        public ProductOptionValue(){ Stocks = new List<ComplexStock>(); }
        public ProductOptionValue(ProductOption productOption, string value, float additionalPrice):this()
        {
            ProductOption = productOption ?? throw new ArgumentNullException(nameof(productOption));
            Value = value ?? throw new ArgumentNullException(nameof(value));
            this.AdditionalPrice = additionalPrice;
        }
        public ProductOptionValue(int id, ProductOption productOption, string value, float additionalPrice):this(productOption, value, additionalPrice)
        {
            Id = id;
        }

        public object Clone()
        {
            return new ProductOptionValue(this.Id,this.ProductOption,this.Value,this.AdditionalPrice);
        }
    }
}