﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MiTienda.Models
{
    public class Stock
    {
        public int Id { get; set; }
        [Min(0)]
        public int Units { get; set; }
        public Product Product { get; set; }

        public Stock()
        {
        }
        public Stock(int units)
        {
            Units = units;
        }
        public Stock(int id, int units)
        {
            Id = id;
            Units = units;
        }
    }
}