/****** Script para el comando SelectTopNRows de SSMS  ******/
delete from OrderLines;
DBCC CHECKIDENT ('mitienda.dbo.OrderLines',RESEED,0)

delete from Orders;
DBCC CHECKIDENT ('mitienda.dbo.Orders',RESEED,0)

delete from ProductOptionValueComplexStocks;

delete from ComplexStocks;
DBCC CHECKIDENT ('mitienda.dbo.ComplexStocks',RESEED,0)

delete from Stocks;
DBCC CHECKIDENT ('mitienda.dbo.Stocks',RESEED,0)

delete from ProductOptionValues;
DBCC CHECKIDENT ('mitienda.dbo.ProductOptionValues',RESEED,0)

delete from ProductOptions;
DBCC CHECKIDENT ('mitienda.dbo.ProductOptions',RESEED,0)

delete from Products;
DBCC CHECKIDENT ('mitienda.dbo.products',RESEED,0)

delete from Categories;
DBCC CHECKIDENT ('mitienda.dbo.categories',RESEED,0)
